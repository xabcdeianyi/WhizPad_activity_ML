package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type BedSensor struct {
	DeviceId string `json:"device_id" bson:"device_id"`
	Epoch    int64  `json:"epoch" bson:"epoch"`
	Bed      Bed    `json:"bed" bson:"bed"`
	PatBed   string `json:"patBed"`
	PatID    string `json:"patId"`
}

type Bed struct {
	DataType       int    `json:"dataType" bson:"dataType"`
	Event          int    `json:"event" bson:"event"`
	Sensors        string `json:"sensors" bson:"sensors"`
	Activity       int    `json:"activity" bson:"activity"`
	ActivityTop    int    `json:"activityTop" bson:"activityTop"`
	ActivityMiddle int    `json:"activityMiddle" bson:"activityMiddle"`
	ActivityDown   int    `json:"activityDown" bson:"activityDown"`
	Activity1Min   int    `json:"activity_1min" bson:"activity_1min"`
	SleepStatus    int    `json:"sleepStatus" bson:"sleepStatus"`
	PressedTime    int    `json:"pressedTime" bson:"pressedTime"`
}

//1661212800000 2022/08/23 08:00  1661212860000

func main() {
	data0823, err := ioutil.ReadFile("./WhizPadEvent0823.json")
	if err != nil {
		return
	}

	dataBed0823 := make([]BedSensor, 0)
	filterData := make([]int, 0)

	if err := json.Unmarshal(data0823, &dataBed0823); err != nil {
		return
	}

	//for i, _ := range dataBed0823 {
	//	if dataBed0823[i].Bed.DataType != 0 {
	//		//filterData = append(filterData, dataBed0823[i])
	//	}
	//}

	for i := 0; i < 1440; i++ {
		v := make([]BedSensor, 0)
		for _, sensor := range dataBed0823 {
			if sensor.Epoch >= int64(1661212800000+i*600) && sensor.Epoch < int64(1661212860000+i*600) {
				v = append(v, sensor)
			}
		}
		if len(v) == 0 {
			filterData = append(filterData, 0)
		} else {
			filterData = append(filterData, v[0].Bed.Activity1Min)
			fmt.Println(v[0].Bed)
		}
		//var lAct int
		//
		//for _, value := range v {
		//	lAct = value.Bed.Activity1Min
		//	if value.Bed.Activity1Min >= lAct {
		//		lAct = value.Bed.Activity1Min
		//	}
		//}
		//fmt.Println(lAct)
	}

	fmt.Println(filterData)

}
